package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Subject;



public interface SubjectRepository<T extends Subject> extends CrudRepository<T, Long>  {

    //public T findByTitleSubject(String titleSubject);



    @PreAuthorize("hasRole('ROLE_MANAGER') or (hasRole('ROLE_TEACHER') and #subject?.teacherSubject?.lastName == authentication?.name)")
    Subject save(@Param("subject") Subject subject);


    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long id);


    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("subject") Subject subject);


}
