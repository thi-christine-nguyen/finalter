package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Student;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.repositories.SubjectRepository;
import um.fds.agl.ter22.repositories.TeacherRepository;

import java.util.Optional;

@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;
    private TeacherRepository teacherRepository;

    public Iterable<Subject> getSubjects() {
        return subjectRepository.findAll();
    }

    public void deleteSubject(final Long id) {
        subjectRepository.deleteById(id);
    }

    public Subject saveSubject(Subject subject) {
        Subject savedSubject = subjectRepository.save(subject);
        return savedSubject;
    }

    public Optional<Subject> findById(long id) {
        return subjectRepository.findById(id);
    }
}
