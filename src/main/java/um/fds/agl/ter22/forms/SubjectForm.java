package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.repositories.TeacherRepository;

public class SubjectForm {

    private String titleSubject;
    private long id;
    private Teacher teacherSubject;

    private String secondaryTeachers;

    public SubjectForm(long id, String titleSubject, Teacher teacherSubject) {
        this.id = id;
        this.titleSubject = titleSubject;
        this.teacherSubject = teacherSubject;
    }

    public SubjectForm(String titleSubject, Teacher teacherSubject) {
        this.titleSubject = titleSubject;
        this.teacherSubject = teacherSubject;
    }

    public SubjectForm(String titleSubject, Teacher teacherSubject, String secondaryTeachers) {
        this.titleSubject = titleSubject;
        this.teacherSubject = teacherSubject;
        this.secondaryTeachers = secondaryTeachers;
    }

    public SubjectForm() {
    }


    public String getTitleSubject() {
        return titleSubject;
    }

    public void setTitleSubject(String titleSubject) {
        this.titleSubject = titleSubject;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Teacher getTeacherSubject() {
        return teacherSubject;
    }

    public void setTeacherSubject(Teacher teacherSubject) {
        this.teacherSubject = teacherSubject;
    }

    public String getSecondaryTeachers() {
        return secondaryTeachers;
    }

    public void setSecondaryTeachers(String secondaryTeachers) {
        this.secondaryTeachers = secondaryTeachers;
    }

    @Override
    public String toString() {
        return "SubjectForm{" +
                "titleSubject='" + titleSubject + '\'' +
                ", id=" + id +
                ", teacherSubject=" + teacherSubject +
                ", secondaryTeachers='" + secondaryTeachers + '\'' +
                '}';
    }
}
