package seleniumTests.base;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TeacherTest extends BaseForTests{

    void addTeacherAccess(){
        driver.findElement(By.linkText("Teachers List")).click();
        driver.findElement(By.linkText("Add Teachers")).click();
    }
    @Test
    void teacherAccessAddTeacherError()  {
        login("Turing", "turing");
        driver.findElement(By.linkText("Teachers List")).click();
        String listeAvant = driver.findElement(By.tagName("body")).getText();
        driver.findElement(By.linkText("Add Teachers")).click();
        driver.findElement(By.linkText("Go back to home page")).click();
        driver.findElement(By.linkText("Teachers List")).click();
        assertEquals(listeAvant, driver.findElement(By.tagName("body")).getText().toString());
    }

    void addTeacherForm(String firstName, String lastName)  {

        WebElement firstNameField = driver.findElement(By.id("firstName"));
        WebElement lastNameField = driver.findElement(By.id("lastName"));
        WebElement submitButton = driver.findElement(By.cssSelector("[type=submit]"));

        write(firstNameField, firstName);
        write(lastNameField, lastName);

        submitButton.click();

    }

    @Test
    void addTeacherByChef() throws IOException {
        login("Chef", "mdp");
        addTeacherAccess();
        addTeacherForm("aaa", "bbb");

        WebElement table = driver.findElement(By.tagName("tbody"));

        assertTrue(table.getText().contains("aaa"));
        assertTrue(table.getText().contains("bbb"));
        screenshot("patate");
    }

}
