package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.services.TeacherService;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private TeacherService teacherService;



    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {
        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        assumingThat(teacherService.getTeacher(10l).isEmpty(), () -> {
            MvcResult result = mvc.perform(post("/addTeacher")
                            .param("firstName", "Anne-Marie")
                            .param("lastName", "Kermarrec")
                            .param("id", "10")
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();
            assertNotNull(teacherService.findByName("Kermarrec"));
        });




// il faut ici vérifier que le nouvel enseignant a bien été ajouté
    }
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        Optional<Teacher> teacher = teacherService.getTeacher(2l);


        assumingThat(!teacherService.getTeacher(2l).isEmpty(), () -> {
                    MvcResult result = mvc.perform(post("/addTeacher")
                                    .param("firstName", "Ada")
                                    .param("lastName", "NotLovelace")
                                    .param("id", "2")//attribution d'id ne fonctionne pas
                            )
                            .andExpect(status().is3xxRedirection())
                            .andReturn();
        });


        assertNotNull(teacherService.findByName("NotLovelace"));



    }



}